public class ProjectStatusUpdateBatch implements Database.Batchable<SObject>, Database.Stateful
{
    private List<Id> failedProjectIds = new List<Id>();

    @testvisible private static final String PROJECT_STATUS_ACTIVE = 'Active';
    private static final String EMAIL_ATTCH_FILE_NAME = 'failedProjectIds.csv';
    private static final String COLUMN_NAME = 'Failed Project Id \n';

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, Start_Date__c, Status__c FROM Project__c]);
    }
  
    public void execute(Database.BatchableContext bc, List<Project__c> projects)
    {
        List<Project__c> projectsToUpdate = new List<Project__c>();

        for(Project__c project : projects)
        {
            if ((project.Start_Date__c < System.now()) && project.Status__c != PROJECT_STATUS_ACTIVE)
            {
                project.Status__c = PROJECT_STATUS_ACTIVE;
                projectsToUpdate.add(project);
            } 
        }

        Database.SaveResult[] srList = Database.update(projectsToUpdate, false);

        for (Database.SaveResult sr : srList)
        {
            if (!sr.isSuccess())
            {
                failedProjectIds.add(sr.getId());
            }
        }
    }
  
    public void finish(Database.BatchableContext bc)
    {
        Email_Setting__mdt emailSettings = Email_Setting__mdt.getInstance('Projects_Status_Update_Message');

        AsyncApexJob job = [
            SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob 
            WHERE Id = :bc.getJobId()];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{ emailSettings.Email_Address__c };
        mail.setToAddresses(toAddresses);
        mail.setSubject(String.format(emailSettings.Email_Subject__c, new String[]{ job.Status }));
        String failedProjectsText = '';

        if (!failedProjectIds.isEmpty())
        {
            for (Id id : failedProjectIds)
            {
                failedProjectsText += id + '\n';
            }

            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(generateStringToCreateCSV());
            csvAttc.setFileName(EMAIL_ATTCH_FILE_NAME);
            csvAttc.setBody(csvBlob);
            mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        }

        String emailText = String.format(
            emailSettings.Email_Text__c, new String[]{ String.valueOf(job.TotalJobItems), String.valueOf(job.NumberOfErrors), failedProjectsText });
        mail.setPlainTextBody(emailText);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
     }

    private string generateStringToCreateCSV()
    {
        string finalString = '';

        if (!failedProjectIds.isEmpty())
        {
            finalString = COLUMN_NAME;

            for (Id id : failedProjectIds)
            {
                finalString += id + '\n';
            }
        }

        return finalString;
    }
}
