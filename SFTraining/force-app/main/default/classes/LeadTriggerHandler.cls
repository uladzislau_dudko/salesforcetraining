public with sharing class LeadTriggerHandler implements TriggerTemplate.Handler 
{
    private List<Lead>      newValues;
    private Map<Id, Lead>   oldValues;
    
    public void setValues(List<sObject> newValues, Map<Id, sObject> oldValues)
    {
        this.newValues = newValues;
        this.oldValues = (Map<Id, Lead>) oldValues;
    }

    public void handle(TriggerTemplate.TriggerAction action)
    {
        if (action == TriggerTemplate.TriggerAction.BEFOREUPDATE)
        {
            LeadTriggerService.checkPartnerAgreements(this.oldValues, this.newValues);
        }

        if ((action == TriggerTemplate.TriggerAction.BEFOREINSERT) || (action == TriggerTemplate.TriggerAction.BEFOREUPDATE))
        {  
            LeadTriggerService.updateStatus(this.oldValues, this.newValues);
        }

        if (action == TriggerTemplate.TriggerAction.BEFOREDELETE)
        {
            LeadTriggerService.deletePartnerAgreements();
        }

        if ((action == TriggerTemplate.TriggerAction.AFTERINSERT) || (action == TriggerTemplate.TriggerAction.AFTERUPDATE))
        {
            LeadTriggerService.handlePartnerAgreements(this.oldValues, this.newValues);
        }   
    }
}
