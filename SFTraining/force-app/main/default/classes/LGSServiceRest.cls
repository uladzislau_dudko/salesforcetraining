@RestResource(urlMapping='/LGSService/*')
global class LGSServiceRest 
{
    @testvisible private static LGS_Response_Setting__mdt wrongRequestParams = 
        LGS_Response_Setting__mdt.getInstance('Wrong_Params');
    @testvisible private static LGS_Response_Setting__mdt insertFailed = 
        LGS_Response_Setting__mdt.getInstance('Insert_Failed');
    @testvisible private static LGS_Response_Setting__mdt updateFailed = 
        LGS_Response_Setting__mdt.getInstance('Update_Failed');
    @testvisible private static LGS_Response_Setting__mdt insertAssignmentSuccess = 
        LGS_Response_Setting__mdt.getInstance('Insert_and_Assignment_Success');
    @testvisible private static LGS_Response_Setting__mdt updateSuccessWoAssignment = 
        LGS_Response_Setting__mdt.getInstance('Update_Success_without_Assignment');

    public class LGSAccount
    {
        public String sfId;
        public String ownerId;
        public String name;
        public String website;
        public String active;

        public LGSAccount(Account account)
        {
            this.sfId = account.Id;
            this.ownerId = account.OwnerId;
            this.name = account.Name;
            this.website = account.Website;
            this.active = account.Active__c;            
        }
    }

    public class LGSOpportunity
    {
        public String sfId;
        public String ownerId;
        public String stageName;
        public Datetime lastModifiedDate;

        public LGSOpportunity(Opportunity opp)
        {
            this.sfId = opp.Id;
            this.ownerId = opp.OwnerId;
            this.stageName = opp.StageName;
            this.lastModifiedDate = opp.LastModifiedDate;          
        }
    }

    public class LGSLead
    {
        public String sfId;
        public String ownerId;
        public String lgsId;
        public String convertedOppId;
        public Datetime lastModifiedDate;
        public Datetime createdDate;
        public String createdById;
        public String firstName;
        public String lastName;
        public String company;
        public String status;
        public String website;
        public String email;
        public String country;
        public String state;
        public String city;

        public LGSLead(Lead lead)
        {
            this.sfId = lead.Id;
            this.ownerId = lead.OwnerId;
            this.lgsId = lead.LGS_Id__c;
            this.convertedOppId = lead.ConvertedOpportunityId;
            this.lastModifiedDate = lead.LastModifiedDate;
            this.createdDate = lead.CreatedDate;
            this.createdById = lead.CreatedById;
            this.firstName = lead.FirstName;
            this.lastName = lead.LastName;
            this.company = lead.Company;
            this.status = lead.Status;
            this.website = lead.Website;
            this.email = lead.Email;
            this.country = lead.Country;
            this.state = lead.State;
            this.city = lead.City;          
        }
    }

    global class LGSRecords 
    {
        public List<LGSAccount> accounts = new List<LGSAccount>();
        public List<LGSLead> leads = new List<LGSLead>();
        public List<LGSOpportunity> opportunities = new List<LGSOpportunity>();

        public void getTodayRecords()
        {
            Set<Id> recordIdsWNewAttchs = new Set<Id>();

            List<Note> newNotes = [
                SELECT Parent.Id, LastModifiedDate
                FROM Note 
                WHERE LastModifiedDate = today];

            for (Note note : newNotes)
            {
                recordIdsWNewAttchs.add(note.Parent.Id);
            }

            List<ContentVersion> newAttchs = [
                SELECT FirstPublishLocationId  
                FROM ContentVersion 
                WHERE LastModifiedDate = today];

            for (ContentVersion attch : newAttchs)
            {
                recordIdsWNewAttchs.add(attch.FirstPublishLocationId);
            }

            List<Account> accountsToday = [
                SELECT Id, OwnerId, Name, Website, Active__c, LastModifiedDate
                FROM Account
                WHERE LastModifiedDate = today
                    OR Id IN :recordIdsWNewAttchs];

            for (Account a : accountsToday)
            {
                accounts.add(new LGSAccount(a));
            }

            List<Opportunity> oppsToday = [
                SELECT Id, OwnerId, StageName, LastModifiedDate
                FROM Opportunity
                WHERE LastModifiedDate = today
                    OR Id IN :recordIdsWNewAttchs];

            for (Opportunity opp : oppsToday)
            {
                opportunities.add(new LGSOpportunity(opp));
            }

            List<Lead> leadsToday = [
                SELECT Id, OwnerId, LGS_Id__c, ConvertedOpportunityId, LastModifiedDate, CreatedDate, CreatedById,
                    FirstName, LastName, Company, Status, Website, Email, Country, State, City
                FROM Lead
                WHERE LastModifiedDate = today
                    OR Id IN :recordIdsWNewAttchs];
            
            for (Lead lead : leadsToday)
            {
                leads.add(new LGSLead(lead));
            }
        }
    }

    global class LGSRequest
    {
        global List<LGSLeadRequest> leads;
    }

    global class LGSLeadRequest
    {
        public String lgsId;
        public String firstName;
        public String lastName;
        public String title;
        public String company;
        public String industry;
        public String status;
        public String website;
        public String email;
        public String country;
        public String state;
        public String city;
    }

    global class LGSResponse
    {
        public List<LGSLeadResponse> leads;

        public LGSResponse(List<LGSLeadResponse> leadsUpserted)
        {
            leads = leadsUpserted;
        }
    }

    global class LGSLeadResponse
    {
        public String sfId;
        public String lgsId;
        public String code;
        public String codeDescription;
        public Datetime lastModifiedDate;
        public String status;

        public LGSLeadResponse(Lead lead, String code, String codeDescription)
        {
            if (lead != null)
            {
                this.sfId = lead.Id;
                this.lgsId = lead.LGS_Id__c;
                this.lastModifiedDate = lead.LastModifiedDate;
                this.status = lead.Status;
            }

            this.code = code;
            this.codeDescription = codeDescription;
        }

        public LGSLeadResponse(String code, String codeDescription)
        {
            this.code = code;
            this.codeDescription = codeDescription;
        }
    }

    @HttpGet
    global static LGSRecords getLgsRecords() 
    { 
        LGSRecords lgsRecords = new LGSRecords();
        lgsRecords.getTodayRecords();

        return lgsRecords;
    }

    @HttpPut
    global static LGSResponse upsertLeads(LGSRequest request) 
    {
        List<LGSLeadResponse> leadResponses = new List<LGSLeadResponse>();
        Map<String,LGSLeadRequest> lgsId2LeadRequest = new Map<String,LGSLeadRequest>();
        Set<String> leadLgsIds = new Set<String>();

        for (LGSLeadRequest leadRequest : request.leads)
        {
            if (leadRequest == null || String.isBlank(leadRequest.lgsId) || String.isBlank(leadRequest.lastName)
                || String.isBlank(leadRequest.title) || String.isBlank(leadRequest.company) 
                || String.isBlank(leadRequest.status))
            {
                LGSLeadResponse leadResponse = new LGSLeadResponse(
                    wrongRequestParams.Code__c, wrongRequestParams.Code_Description__c);
                leadResponses.add(leadResponse);
            }
            else
            {
                leadLgsIds.add(leadRequest.lgsId);
                lgsId2LeadRequest.put(leadRequest.lgsId, leadRequest);               
            }
        }

        List<Lead> leadsUpdate = [
            SELECT LGS_Id__c, FirstName, LastName, Title, Company,
                 Industry, Status, Website, Email, Country, State, City, LastModifiedDate
            FROM Lead 
            WHERE LGS_Id__c IN :leadLgsIds];

        for (Lead lead : leadsUpdate)
        {
            leadLgsIds.remove(lead.LGS_Id__c);
            LGSLeadRequest leadReq = lgsId2LeadRequest.get(lead.LGS_Id__c);

            lead.FirstName = leadReq.firstName;
            lead.LastName = leadReq.lastName;
            lead.Title = leadReq.title;
            lead.Company = leadReq.company;
            lead.Industry = leadReq.industry;
            lead.Status = leadReq.status;
            lead.Website = leadReq.website;
            lead.Email = leadReq.email;
            lead.Country = leadReq.country;
            lead.State = leadReq.state;
            lead.City = leadReq.city;
        }

        List<Lead> leadsInsert = new List<Lead>();

        for (String lgsId : leadLgsIds)
        {
            LGSLeadRequest leadReq = lgsId2LeadRequest.get(lgsId);

            Lead lead = new Lead(LGS_Id__c = leadReq.lgsId, FirstName = leadReq.firstName,
                LastName = leadReq.lastName, Title = leadReq.title, Company = leadReq.company,
                Industry = leadReq.industry, Status = leadReq.status, Website = leadReq.website,
                Email = leadReq.email, Country = leadReq.country, State = leadReq.state,
                City = leadReq.city);

            leadsInsert.add(lead);
        }

        Database.DMLOptions dmlOpt = new Database.DMLOptions();
        dmlOpt.assignmentRuleHeader.useDefaultRule= false; 
        dmlOpt.DuplicateRuleHeader.AllowSave = true; 

        List<Database.SaveResult> updateResults = Database.update(leadsUpdate, dmlOpt);
        Map<Id, Lead> id2leadsUpdate = new Map<Id, Lead>(leadsUpdate);

        dmlOpt.assignmentRuleHeader.useDefaultRule= true;

        List<Database.SaveResult> insertResults = Database.insert(leadsInsert, dmlOpt);
        Map<Id, Lead> id2leadsInsert = new Map<Id, Lead>(leadsInsert);

        for (Database.SaveResult updRes : updateResults)
        {
            Lead lead = id2leadsUpdate.get(updRes.getId());

            if (updRes.isSuccess())
            {
                LGSLeadResponse leadResponse = new LGSLeadResponse(
                    lead, updateSuccessWoAssignment.Code__c, 
                    updateSuccessWoAssignment.Code_Description__c);
                leadResponses.add(leadResponse);                
            }
            else 
            {
                LGSLeadResponse leadResponse = new LGSLeadResponse(
                    lead, updateFailed.Code__c, updRes.getErrors()[0].getMessage());
                leadResponses.add(leadResponse);                
            }
        }

        for (Database.SaveResult insertRes : insertResults)
        {
            Lead lead = id2leadsInsert.get(insertRes.getId());

            if (insertRes.isSuccess())
            {
                LGSLeadResponse leadResponse = new LGSLeadResponse(
                    lead, insertAssignmentSuccess.Code__c, 
                    insertAssignmentSuccess.Code_Description__c);
                leadResponses.add(leadResponse);                
            }
            else 
            {
                LGSLeadResponse leadResponse = new LGSLeadResponse(
                    lead, insertFailed.Code__c, insertRes.getErrors()[0].getMessage());
                leadResponses.add(leadResponse);                
            }
        }

        LGSResponse response = new LGSResponse(leadResponses);

        return response;  
    }
}
