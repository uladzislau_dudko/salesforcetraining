@isTest
private class LeadTriggerTest {

    private static final String LEAD_COMPANY = 'Test Company';
    private static final String LEAD_SOURCE_EMAIL = 'Email';
    private static final Integer TEST_COUNT = 10;
    
    private static List<Lead> createLeads(Integer leadsCount, String leadSource)
    {
        List<Lead> leads = new List<Lead>();

        for (Integer i = 0; i < leadsCount; i++) 
        {
            Lead lead = new Lead(LastName = 'Test Lead ' + String.valueOf(i), Company = LEAD_COMPANY, 
                LeadSource = leadSource, Status = 'Open - Not Contacted', No_Website__c = true);
            leads.add(lead);
        }

        return leads;
    }

    @isTest static void testCheckPartnerAgreements()
    {
        List<Lead> leads = createLeads(TEST_COUNT, LeadTriggerService.LEAD_SOURCE_PARTNER);
        insert leads;

        for (Lead lead : leads) {
            lead.Status = LeadTriggerService.LEAD_STATUS_CLOSED_CONVERTED;
        }

        Test.startTest();
        Database.SaveResult[] srList = Database.update(leads, false);
        Test.stopTest();
        
        System.assert(!srList[0].isSuccess(), 'Lead was not updated');
        System.assert(srList[0].getErrors().size() > 0, 'Error accurred');
        System.assertEquals(System.Label.LEAD_STATUS_CONVERSION_ERROR, srList[0].getErrors()[0].getMessage(), 'Not expected error');
                             
    }

    @isTest static void testUpdateStatusOnLeadsInsert()
    {
        List<Lead> leads = createLeads(TEST_COUNT, LeadTriggerService.LEAD_SOURCE_WEB);

        Test.startTest();
        insert leads;
        Test.stopTest();

        List<Lead> insertedLeads = [
            SELECT Company, LeadSource, Status
            FROM Lead 
            WHERE Company = :LEAD_COMPANY
                AND LeadSource = :LeadTriggerService.LEAD_SOURCE_WEB
                AND Status = :LeadTriggerService.LEAD_STATUS_WORIKNG_CONTACTED];
        
        System.assertEquals(TEST_COUNT, insertedLeads.size(), 'Status Was not changed to Working - Contacted');
    }

    @isTest static void testUpdateStatusOnLeadsUpdate()
    {
        List<Lead> leads = createLeads(TEST_COUNT, LEAD_SOURCE_EMAIL);
        insert leads;

        for (Lead lead : leads) {
            lead.LeadSource = LeadTriggerService.LEAD_SOURCE_WEB;
        }
        
        Test.startTest();
        update leads;
        Test.stopTest();

        List<Lead> updatedLeads = [
            SELECT Company, LeadSource, Status
            FROM Lead 
            WHERE Company = :LEAD_COMPANY
                AND LeadSource = :LeadTriggerService.LEAD_SOURCE_WEB
                AND Status = :LeadTriggerService.LEAD_STATUS_WORIKNG_CONTACTED];
        
        System.assertEquals(TEST_COUNT, updatedLeads.size(), 'Status Was not changed to Working - Contacted');
    }

    @isTest static void testDeletePartnerAgreements()
    {
        List<Lead> leads = createLeads(TEST_COUNT, LeadTriggerService.LEAD_SOURCE_PARTNER);
        insert leads;

        List<Lead> insertedLeads = [
            SELECT Company, LeadSource
            FROM Lead 
            WHERE Company = :LEAD_COMPANY
                AND LeadSource = :LeadTriggerService.LEAD_SOURCE_PARTNER];

        Set<Id> leadIds = (new Map<Id, Lead>(insertedLeads)).keySet();

        Test.startTest();
        delete insertedLeads;
        Test.stopTest();

        List<Partner_Agreement__c> newPA = [SELECT Lead__c FROM Partner_Agreement__c WHERE Lead__c IN :leadIds];
        System.assertEquals(0, newPA.size(), 'Partner Agreement records was not deleted');
    }

    @isTest static void testHandlePartnerAgreementsOnLeadsInsert()
    {
        List<Lead> leads = createLeads(TEST_COUNT, LeadTriggerService.LEAD_SOURCE_PARTNER);

        Test.startTest();
        insert leads;
        Test.stopTest();

        List<Lead> insertedLeads = [
            SELECT Company, LeadSource
            FROM Lead 
            WHERE Company = :LEAD_COMPANY
                AND LeadSource = :LeadTriggerService.LEAD_SOURCE_PARTNER];

        Set<Id> leadIds = (new Map<id, Lead>(insertedLeads)).keySet();

        List<Partner_Agreement__c> newPA = [SELECT Lead__c FROM Partner_Agreement__c WHERE Lead__c IN :leadIds];  
        System.assertEquals(TEST_COUNT, newPA.size(), 'Partner Agreement records were not inserted');
    }

    @isTest static void testHandlePartnerAgreementsOnLeadsUpdateEmailToPartner()
    {
        List<Lead> leads = createLeads(TEST_COUNT, LEAD_SOURCE_EMAIL);
        insert leads;

        for (Lead lead : leads) {
            lead.LeadSource = LeadTriggerService.LEAD_SOURCE_PARTNER;
        }

        Test.startTest();
        update leads;
        Test.stopTest();

        List<Lead> updatedLeads = [
            SELECT Company, LeadSource
            FROM Lead 
            WHERE Company = :LEAD_COMPANY
                AND LeadSource = :LeadTriggerService.LEAD_SOURCE_PARTNER];

        Set<Id> leadIds = (new Map<id, Lead>(updatedLeads)).keySet();

        List<Partner_Agreement__c> newPA = [SELECT Lead__c FROM Partner_Agreement__c WHERE Lead__c IN :leadIds];
        System.assertEquals(TEST_COUNT, newPA.size(), 'Partner Agreement record was not inserted');
    }

    @isTest static void testHandlePartnerAgreementsOnLeadsUpdatePartnerToEmail()
    {
        List<Lead> leads = createLeads(TEST_COUNT, LeadTriggerService.LEAD_SOURCE_PARTNER);
        insert leads;

        for (Lead lead : leads) {
            lead.LeadSource = LEAD_SOURCE_EMAIL;
        }

        Test.startTest();
        update leads;
        Test.stopTest();

        List<Lead> updatedLeads = [
            SELECT Company, LeadSource
            FROM Lead 
            WHERE Company = :LEAD_COMPANY
                AND LeadSource = :LeadTriggerService.LEAD_SOURCE_PARTNER];

        Set<Id> leadIds = (new Map<id, Lead>(updatedLeads)).keySet();

        List<Partner_Agreement__c> newPA = [SELECT Lead__c FROM Partner_Agreement__c WHERE Lead__c IN :leadIds];
        System.assertEquals(0, newPA.size(), 'Partner Agreement record was not deleted');
    }
}
