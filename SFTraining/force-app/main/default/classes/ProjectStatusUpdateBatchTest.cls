@isTest
private class ProjectStatusUpdateBatchTest 
{
    private static final String PROJECT_NAME_ACTIVE = 'Test Project active';
    private static final String PROJECT_NAME_NOT_ACTIVE = 'Test Project not active';
    private static final String PROJECT_STATUS_PLANNED = 'Planned';
    private static final Integer NUM_PROJECTS = 10;

    @testSetup
    static void setup() 
    {
        List<Account> accounts = new List<Account>();
        List<Project__c> projects = new List<Project__c>();

        for (Integer i = 0; i < NUM_PROJECTS; i++)
        {
            accounts.add(new Account(name = 'Test Account' + String.valueOf(i)));
        }

        insert accounts;

        for (Account account : accounts) 
        {
            projects.add(new Project__c(Name = PROJECT_NAME_ACTIVE,
                Start_Date__c = Date.today().addDays(-1), Account__c = account.id, Status__c = PROJECT_STATUS_PLANNED));
            projects.add(new Project__c(Name = PROJECT_NAME_NOT_ACTIVE,
                Start_Date__c=Date.today().addDays(1), Account__c = account.id, Status__c = PROJECT_STATUS_PLANNED));    
        }
        
        insert projects;
    }
    @isTest static void testProjectStatusUpdate() 
    {
        Test.startTest();
        Id batchId = Database.executeBatch(new ProjectStatusUpdateBatch());
        Test.stopTest();

        System.assertEquals(NUM_PROJECTS, [SELECT count() 
                                           FROM Project__c 
                                           WHERE Name = :PROJECT_NAME_ACTIVE 
                                               AND Status__c = :ProjectStatusUpdateBatch.PROJECT_STATUS_ACTIVE], 
                            String.valueOf(NUM_PROJECTS) + ' Projects not set to Active Status');

        System.assertEquals(NUM_PROJECTS, [SELECT count() 
                                           FROM Project__c 
                                           WHERE Name = :PROJECT_NAME_NOT_ACTIVE 
                                               AND Status__c = :PROJECT_STATUS_PLANNED],
                            String.valueOf(NUM_PROJECTS) + ' Projects shouldn\'t be set to Active Status');
    }
}
