public without sharing class LeadTriggerService 
{
    @testvisible private static final String LEAD_SOURCE_WEB = 'Web';
    @testvisible private static final String LEAD_SOURCE_PARTNER = 'Partner';
    @testvisible private static final String LEAD_SOURCE_CHANNEL_WEBSITE = 'Website';
    @testvisible private static final String LEAD_STATUS_WORIKNG_CONTACTED = 'Working - Contacted';
    @testvisible private static final String LEAD_STATUS_CLOSED_CONVERTED = 'Closed - Converted';

    private static Boolean stopProcessing = false;

    /*
     *    @description      This method is needed to update Lead Status to 'Working - Contacted' if LeadSource was changed to 'Web'
     *                      or LeadSource Channel was changed to Website
     */
    public static void updateStatus(Map<Id, Lead> oldLeads, List<Lead> newLeads)
    {
        if (stopProcessing)
        {
            return;
        }

        Lead oldLead;

        for (Lead newLead : newLeads)
        {
            if (oldLeads != null)
            {
                oldLead = oldLeads.get(newLead.Id);
            }

            if ((Trigger.isInsert && (newLead.LeadSource == LEAD_SOURCE_WEB || newLead.LeadSource_Channel__c == LEAD_SOURCE_CHANNEL_WEBSITE)) 
                    || (Trigger.isUpdate && ((newLead.LeadSource == LEAD_SOURCE_WEB && oldLead.LeadSource != LEAD_SOURCE_WEB) 
                        || (newLead.LeadSource_Channel__c == LEAD_SOURCE_CHANNEL_WEBSITE && oldLead.LeadSource_Channel__c != LEAD_SOURCE_CHANNEL_WEBSITE))))
            {
                newLead.Status = LEAD_STATUS_WORIKNG_CONTACTED;
            }   
        }         
    }

    /*
     *    @description      This method is needed to prevent convertation Lead Status to 'Closed - Converted' 
     *                      if field Terms of Cooperation is not filled in on related Partner Agreement record
     */
    public static void checkPartnerAgreements(Map<Id, Lead> oldLeads, List<Lead> newLeads)
    {
        if (stopProcessing)
        {   
            return;
        }

        List<Partner_Agreement__c> partnerAgreementsWithoutTermsOfCooperation = [
            SELECT Lead__c, Terms_of_Cooperation__c 
            FROM Partner_Agreement__c 
            WHERE Lead__c IN :oldLeads.keySet() 
                AND Terms_of_Cooperation__c = null];

        Map<Id, Lead> leadsWithoutTermsOfCooperation = new Map<Id, Lead>();

        for (Partner_Agreement__c pa : partnerAgreementsWithoutTermsOfCooperation)
        {
            leadsWithoutTermsOfCooperation.put(pa.Lead__c, oldLeads.get(pa.Lead__c));
        }

        for (Lead newLead : newLeads)
        {
            Lead oldLead = leadsWithoutTermsOfCooperation.get(newLead.Id);

            if (oldLead != null && newLead.Status == LEAD_STATUS_CLOSED_CONVERTED && oldLead.Status != LEAD_STATUS_CLOSED_CONVERTED)
            {
                newLead.addError(System.Label.LEAD_STATUS_CONVERSION_ERROR);
                stopProcessing = true;
                return;
            }            
        }
    }
    
    /*
     *    @description      This method is needed to create PartnerAgrement record if field LeadSource was changed to Partner
     *                      and delete related PartnerAgrement record if field LeadSource was changed from Partner to another value
     */
    public static void handlePartnerAgreements(Map<Id, Lead> oldLeads, List<Lead> newLeads)
    {
        if (stopProcessing) 
        {
            return;           
        }

        Set<Id> leadIdsForPartnerAgreementInsert = new Set<Id>();
        Set<Id> leadIdsForPartnerAgreementDelete = new Set<Id>();
        Lead oldLead;

        for (Lead newLead : newLeads)
        {
            if (oldLeads != null)
            {
                oldLead = oldLeads.get(newLead.Id);
            }

            if ((Trigger.isInsert && (newLead.LeadSource == LEAD_SOURCE_PARTNER))
                    || (Trigger.isUpdate && (newLead.LeadSource == LEAD_SOURCE_PARTNER && oldLead.LeadSource != LEAD_SOURCE_PARTNER)))
            {
                leadIdsForPartnerAgreementInsert.add(newLead.Id);
            }

            if (Trigger.isUpdate && (newLead.LeadSource != LEAD_SOURCE_PARTNER && oldLead.LeadSource == LEAD_SOURCE_PARTNER))
            {
                leadIdsForPartnerAgreementDelete.add(newLead.Id);
            }   
        }

        createPartnerAgreements(leadIdsForPartnerAgreementInsert);
        deletePartnerAgreementsFromDB(leadIdsForPartnerAgreementDelete);
    }

    /*
     *    @description      This method is needed to delete related PartnerAgrement record if Lead is deleted
     */
    public static void deletePartnerAgreements()
    {
        if (stopProcessing)
        {
            return;
        }

        Set<Id> leadIdsForPartnerAgreementDelete = (new Map<Id, SObject>(Trigger.old)).keySet();
        deletePartnerAgreementsFromDB(leadIdsForPartnerAgreementDelete);
    }

    private static void deletePartnerAgreementsFromDB(Set<Id> leadIds)
    {
        if (!leadIds.isEmpty())
        {
            List<Partner_Agreement__c> partnerAgreements = [SELECT Lead__c FROM Partner_Agreement__c WHERE Lead__c IN :leadIds];
            delete partnerAgreements;
        }     
    }

    private static void createPartnerAgreements(Set<Id> leadIds)
    {
        List<Partner_Agreement__c> newPartnerAgreements = new List<Partner_Agreement__c>();

        for (Id id : leadIds)
        {
            Partner_Agreement__c pa = new Partner_Agreement__c(Lead__c = id);
            newPartnerAgreements.add(pa);
        }

        insert newPartnerAgreements;
    }
   
}
