@IsTest
private class LGSServiceRestTest 
{
    private final static Integer TEST_COUNT = 10;

    @isTest static void testUpsertLeads()
    {
        LGSServiceRest.LGSRequest requestSuccess = createTestRequest(true);
        LGSServiceRest.LGSRequest requestFail = createTestRequest(false);

        test.startTest();
        LGSServiceRest.LGSResponse responseInsert = LGSServiceRest.upsertLeads(requestSuccess);
        LGSServiceRest.LGSResponse responseUpdate = LGSServiceRest.upsertLeads(requestSuccess);
        LGSServiceRest.LGSResponse responseFail = LGSServiceRest.upsertLeads(requestFail);
        test.stopTest();

        System.assertEquals(LGSServiceRest.insertAssignmentSuccess.Code__c, 
            responseInsert.leads[0].code, 'Success insert code should be returned');
        System.assertEquals(LGSServiceRest.updateSuccessWoAssignment.Code__c, 
            responseUpdate.leads[0].code, 'Success updste code should be returned');
        System.assertEquals(LGSServiceRest.wrongRequestParams.Code__c, 
            responseFail.leads[0].code, 'Wrong params code should be returned');
    }

    @isTest static void testGetLgsRecords() 
    {
        createTestRecords();
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://itechart-6c-dev-ed.my.salesforce.com/services/apexrest/LGSService/';
        request.httpMethod = 'GET';
        RestContext.request = request;

        Test.startTest();
        LGSServiceRest.LGSRecords lgsRecords = LGSServiceRest.getLgsRecords();
        Test.stopTest();

        System.assert(lgsRecords != null, 'LGSRecords object were created');
        System.assertEquals(
            TEST_COUNT, lgsRecords.accounts.size(), String.valueOf(TEST_COUNT) + ' Accounts should be returned');
        System.assertEquals(
            TEST_COUNT, lgsRecords.leads.size(), String.valueOf(TEST_COUNT) + ' Leads should be returned');
        System.assertEquals(
            TEST_COUNT, lgsRecords.opportunities.size(), String.valueOf(TEST_COUNT) + ' Opps should be returned');
    }

    private static LGSServiceRest.LGSRequest createTestRequest(Boolean hasLgsId)
    {
        LGSServiceRest.LGSRequest request = new LGSServiceRest.LGSRequest();
        List<LGSServiceRest.LGSLeadRequest> leadReqList = new List<LGSServiceRest.LGSLeadRequest>();

        for (Integer i = 0; i < TEST_COUNT; i++) 
        {
            LGSServiceRest.LGSLeadRequest leadReq = new LGSServiceRest.LGSLeadRequest();

            leadReq.lgsId = hasLgsId ? 'Test' + String.valueOf(i) : null;
            leadReq.firstName = 'Test' + String.valueOf(i);
            leadReq.lastName = 'Test' + String.valueOf(i);
            leadReq.title = 'CFO';
            leadReq.company = 'Cadinal Inc.';
            leadReq.industry = null;
            leadReq.status = 'New';
            leadReq.website = 'Test' + String.valueOf(i);
            leadReq.email = 'test@gmail.com';
            leadReq.country = null;
            leadReq.state = null;
            leadReq.city = null;
    
            leadReqList.add(leadReq);            
        }
  
        request.leads = leadReqList;

        return request;
    }

    private static void createTestRecords() 
    {
        List<Account> accounts = new List<Account>();
        List<Lead> leads = new List<Lead>();
        List<Opportunity> opps = new List<Opportunity>();

        for (Integer i = 0; i < TEST_COUNT; i++) {
            Account account = new Account(
                Name='Test account ' + String.valueOf(i), Active__c = 'Yes', Website = 'test.com');
            accounts.add(account);
        }

        for (Integer i = 0; i < TEST_COUNT; i++) {
            Lead lead = new Lead(
                LastName='Test lead ' + String.valueOf(i), Website = 'test.com', Company = 'Test Company');
            leads.add(lead);
        }

        for (Integer i = 0; i < TEST_COUNT; i++) {
            Opportunity opp = new Opportunity(
                Name='Test opp ' + String.valueOf(i), StageName = 'Prospecting', CloseDate = System.today());
            opps.add(opp);
        }

        insert accounts;
        insert leads;
        insert opps;
    } 
}