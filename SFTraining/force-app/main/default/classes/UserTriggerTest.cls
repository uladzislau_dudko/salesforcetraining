@isTest
private class UserTriggerTest {

    private static final Integer TEST_COUNT = 10;
    
    private static List<User> createUsers(Integer usersCount, Id profileId)
    {
        List<User> users = new List<User>();

        for (Integer i = 0; i < usersCount; i++) 
        {
            User user = new User(Alias = 'standt', Email = 'standarduser@testorg.com', 
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey = 'en_US', ProfileId = profileId, 
                TimeZoneSidKey = 'America/Los_Angeles', UserName = String.valueOf(i) + '222111standarduser@testorg.com');

            users.add(user);
        }

        return users;
    }
    @isTest static void testHandlePublicGroupsInsert()
    {
        List<User> users = createUsers(TEST_COUNT, UserTriggerService.bdmSettings.Profile_Id__c);

        Test.startTest();
        insert users;
        Test.stopTest();

        List<GroupMember> groupMembers = [
            SELECT GroupId
            FROM GroupMember
            WHERE  GroupId = :UserTriggerService.bdmSettings.Public_Group_Id__c
        ];

        System.assertEquals(TEST_COUNT, groupMembers.size(), 'Group Members were not created');
    }

    @isTest static void testHandlePublicGroupsUpdate()
    {
        List<User> users = createUsers(TEST_COUNT, UserTriggerService.bdmSettings.Profile_Id__c);
        insert users;

        for (User user : users)
        {
            user.ProfileId = UserTriggerService.deliveryManagerSettings.Profile_Id__c;
        }

        Test.startTest();
        update users;
        Test.stopTest();

        List<GroupMember> groupMembersBdm = [
            SELECT GroupId
            FROM GroupMember
            WHERE  GroupId = :UserTriggerService.bdmSettings.Public_Group_Id__c
        ];

        List<GroupMember> groupMembersdeliveryManager = [
            SELECT GroupId
            FROM GroupMember
            WHERE  GroupId = :UserTriggerService.deliveryManagerSettings.Public_Group_Id__c
        ];

        System.assertEquals(TEST_COUNT, groupMembersdeliveryManager.size(), 
            'Group Members of Delivery Managers group were not created');
        System.assertEquals(0, groupMembersBdm.size(), 
            'Group Members of BDM should be deleted');
    }

    @isTest static void testHandlePublicGroupsUpdateIsActive()
    {
        List<User> users = createUsers(TEST_COUNT, UserTriggerService.bdmSettings.Profile_Id__c);
        insert users;

        for (User user : users)
        {
            user.isActive = false;
        }

        Test.startTest();
        update users;
        Test.stopTest();

        List<GroupMember> groupMembers = [
            SELECT GroupId
            FROM GroupMember
            WHERE  GroupId = :UserTriggerService.bdmSettings.Public_Group_Id__c
        ];

        System.assertEquals(0, groupMembers.size(), 'Group Members of BDM should be deleted');
    }

}
