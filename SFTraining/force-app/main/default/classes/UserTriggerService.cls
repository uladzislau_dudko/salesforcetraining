public with sharing class UserTriggerService 
{
    @testvisible private static Profile_setting__mdt bdmSettings = Profile_setting__mdt.getInstance('BDM_Profile');
    @testvisible private static Profile_setting__mdt deliveryManagerSettings = Profile_setting__mdt.getInstance('Delivery_Manager_Profile');
    @testvisible private static Profile_setting__mdt marketingSettings = Profile_setting__mdt.getInstance('Marketing_Profile');

    static Set<Id> bdmUserIdsDelete = new Set<Id>();
    static Set<Id> deliveryManagerUserIdsDelete = new Set<Id>();
    static Set<Id> marketingUserIdsDelete = new Set<Id>();

    public static void handlePublicGroups(Map<Id, User> oldUsers, List<User> newUsers)
    {
        List<GroupMember> groupMembersInsert = new List<GroupMember>();
        User oldUser;

        for (User newUser : newUsers)
        {
            if (oldUsers != null)
            {
                oldUser = oldUsers.get(newUser.Id);
            }
            
            if (newUser.isActive && ( Trigger.isInsert || (Trigger.isUpdate && (newUser.ProfileId != oldUser.ProfileId))))
            {
                GroupMember groupMember = createGroupMember(newUser);

                if (groupMember != null)
                {
                    groupMembersInsert.add(groupMember);
                }
            }

            if (Trigger.isUpdate)
            {
                fillSetsUserIdsForDelete(newUser, oldUser);               
            }
        }
        
        if (!groupMembersInsert.isEmpty())
        {
            insert groupMembersInsert;
        }

        if (!bdmUserIdsDelete.isEmpty() || !deliveryManagerUserIdsDelete.isEmpty() || !marketingUserIdsDelete.isEmpty())
        {
            deleteGroupMembers();
        }   
    }

    private static GroupMember createGroupMember(User user)
    {
        GroupMember groupMember;

        if (user.ProfileId == bdmSettings.Profile_Id__c)
        {
            groupMember = new GroupMember(GroupId = bdmSettings.Public_Group_Id__c, UserOrGroupId = user.Id);
        }

        if (user.ProfileId == deliveryManagerSettings.Profile_Id__c)
        {
            groupMember = new GroupMember(GroupId = deliveryManagerSettings.Public_Group_Id__c, UserOrGroupId = user.Id);
        }

        if (user.ProfileId == marketingSettings.Profile_Id__c)
        {
            groupMember = new GroupMember(GroupId = marketingSettings.Public_Group_Id__c, UserOrGroupId = user.Id);
        }
        
        return groupMember;
    }

    private static void fillSetsUserIdsForDelete(User newUser, User oldUser)
    {
        if (( newUser.ProfileId != bdmSettings.Profile_Id__c 
                && oldUser.ProfileId == bdmSettings.Profile_Id__c)
            || (newUser.ProfileId == bdmSettings.Profile_Id__c && !newUser.IsActive && oldUser.IsActive))
        {
            bdmUserIdsDelete.add(newUser.Id);
        }

        if ((newUser.ProfileId != deliveryManagerSettings.Profile_Id__c 
                && oldUser.ProfileId == deliveryManagerSettings.Profile_Id__c)
            || (newUser.ProfileId == deliveryManagerSettings.Profile_Id__c && !newUser.IsActive && oldUser.IsActive))
        {
            deliveryManagerUserIdsDelete.add(newUser.Id);
        }

        if ((newUser.ProfileId != marketingSettings.Profile_Id__c 
                && oldUser.ProfileId == marketingSettings.Profile_Id__c)
            || (newUser.ProfileId == marketingSettings.Profile_Id__c && !newUser.IsActive && oldUser.IsActive))
        {
            marketingUserIdsDelete.add(newUser.Id);
        } 
    }

    private static void deleteGroupMembers()
    {
        List<String> wherePartsGroupMembersDeleteQuery = new List<String>();

        if (!bdmUserIdsDelete.isEmpty())
        {
            wherePartsGroupMembersDeleteQuery.add(
                'GroupId = \'' + bdmSettings.Public_Group_Id__c + '\' AND UserOrGroupId IN :bdmUserIdsDelete');
        }

        if (!deliveryManagerUserIdsDelete.isEmpty())
        {
            wherePartsGroupMembersDeleteQuery.add(
                'GroupId = \'' + deliveryManagerSettings.Public_Group_Id__c + '\' AND UserOrGroupId IN :deliveryManagerUserIdsDelete');
        }

        if (!marketingUserIdsDelete.isEmpty())
        {
            wherePartsGroupMembersDeleteQuery.add(
                'GroupId = \'' + marketingSettings.Public_Group_Id__c + '\' AND UserOrGroupId IN :marketingUserIdsDelete');
        }

        if (!wherePartsGroupMembersDeleteQuery.isEmpty())
        {
            String groupMembersDeleteQuery = 'SELECT GroupId, UserOrGroupId FROM GroupMember WHERE ';
            Integer size = wherePartsGroupMembersDeleteQuery.size();

            for (Integer i = 0; i < size; i++) 
            {
                if (size > 1)
                {
                    wherePartsGroupMembersDeleteQuery[i] = '(' + wherePartsGroupMembersDeleteQuery[i] + ')';
                }

                groupMembersDeleteQuery += wherePartsGroupMembersDeleteQuery[i] + ((i < size - 1) ? 'OR' : ' ');
            }

            List<GroupMember> groupMembersDelete = Database.query(groupMembersDeleteQuery);
            delete groupMembersDelete;
        }     
    }
}
