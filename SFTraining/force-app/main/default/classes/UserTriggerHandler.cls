public with sharing class UserTriggerHandler implements TriggerTemplate.Handler 
{
    private List<User>      newValues;
    private Map<Id, User>   oldValues;
    
    public void setValues(List<sObject> newValues, Map<Id, sObject> oldValues)
    {
        this.newValues = newValues;
        this.oldValues = (Map<Id, User>) oldValues;
    }

    public void handle(TriggerTemplate.TriggerAction action)
    {
        if ((action == TriggerTemplate.TriggerAction.AFTERINSERT) || (action == TriggerTemplate.TriggerAction.AFTERUPDATE))
        {
            UserTriggerService.handlePublicGroups(this.oldValues, this.newValues);
        }   
    }
}
