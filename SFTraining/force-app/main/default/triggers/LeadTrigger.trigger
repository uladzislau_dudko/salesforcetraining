trigger LeadTrigger on Lead (before insert, before update, before delete, after insert, after update) 
{
    TriggerTemplate.TriggerManager triggerManager = new TriggerTemplate.TriggerManager();
    triggerManager.addHandler(
        new LeadTriggerHandler(), 
        new List<TriggerTemplate.TriggerAction>
        {
            TriggerTemplate.TriggerAction.BEFOREINSERT,
            TriggerTemplate.TriggerAction.BEFOREUPDATE,
            TriggerTemplate.TriggerAction.AFTERINSERT,
            TriggerTemplate.TriggerAction.AFTERUPDATE,
            TriggerTemplate.TriggerAction.BEFOREDELETE
        }
    );
    triggerManager.runHandlers();
}

