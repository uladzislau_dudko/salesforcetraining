trigger UserTrigger on User (after insert, after update, before delete) {

    TriggerTemplate.TriggerManager triggerManager = new TriggerTemplate.TriggerManager();
    triggerManager.addHandler(
        new UserTriggerHandler(), 
        new List<TriggerTemplate.TriggerAction>
        {
            TriggerTemplate.TriggerAction.AFTERINSERT,
            TriggerTemplate.TriggerAction.AFTERUPDATE
        }
    );
    triggerManager.runHandlers();
}