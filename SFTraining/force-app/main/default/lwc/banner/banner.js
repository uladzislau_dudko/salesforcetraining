import { LightningElement, api } from 'lwc';
export default class Banner extends LightningElement 
{
    @api bannerText;
    @api textColor;
    @api textSize;

    renderedCallback() 
    {
        this.template
            .querySelector(".bannerClass")
            .style.color = this.textColor;

        this.template
            .querySelector(".bannerClass")
            .style.fontSize = this.textSize; 
    }
}